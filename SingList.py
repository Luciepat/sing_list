from math import pow

# figure ou how to import shit again
class Person:
    def __init__(self, name):
        self.name = name
        self.sung = 0
        self.songs = []
        self.lapse = 0
        self.value = 0


class SingList:
    def __init__(self):
        self.people = {}
        self.upcoming_list = []

    def AddPerson(self, name):
        if not name in self.people.keys():
            self.people[name] = Person(name)
            return  True
        print("ID is taken\n")
        return False

    def AddSong(self, name, song):
        try:
            self.people[name].songs.append(song)
            self.people[name].lapse = 0

            if not name in self.upcoming_list:
                self.upcoming_list.append(name)

            return True
        except KeyError as e:
            print(e)
            return False

    def PopSong(self):
        person = self.upcoming_list[0]
        try:
            song = self.people[person].songs[0]

            self.people[person].lapse = -1
            self.people[person].sung += 1

            self.people[person].songs.remove(song)
            if self.people[person].songs == []:
                self.upcoming_list.remove(person)
        except KeyError as e:
            print(e)
            return False

        _update_lapse()
        _reevaluate_list()
        return person, song

    def _update_lapse(self):
        for name in self.upcoming_list:
            self.people[name].lapse += 1
            self.people[name].value =  pow(4, (1 / (self.people[name].sung + 1))) * self.people[name].lapse

    def _reevaluate_list(self):
        value = lambda person: self.people[person].value
        self.upcoming_list = sorted(self.upcoming_list, key=value, reverse=True)





